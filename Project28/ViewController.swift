//
//  ViewController.swift
//  Project28
//
//  Created by Danni Brito on 4/13/20.
//  Copyright © 2020 Danni Brito. All rights reserved.
//

import UIKit
import LocalAuthentication

class ViewController: UIViewController {
    @IBOutlet var secret: UITextView!
    
    let password = "danni"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        KeychainWrapper.standard.set(password, forKey: "Password")
        
        title = "Nothing to see here"
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(saveSecretMessage))
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillHideNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(adjustForKeyboard), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
        notificationCenter.addObserver(self, selector: #selector(saveSecretMessage), name: UIApplication.willResignActiveNotification, object: nil)
    }

    @IBAction func authenticateTapped(_ sender: Any) {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identify Yourself!"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { [weak self] (success, authenticationError) in
                DispatchQueue.main.async {
                    if success {
                        self?.unlockSecretMessage()
                    } else {
                        self?.promptForPassword()
                    }
                }
            }
        } else {
            promptForPassword()
        }
    }
    
    @objc func adjustForKeyboard(notification: Notification){
        guard let keyboardValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return }
        
        let keyboardScreenEnd = keyboardValue.cgRectValue
        let keyboardViewEndFrame = view.convert(keyboardScreenEnd, to: view.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            secret.contentInset = .zero
        } else {
            secret.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardViewEndFrame.height - view.safeAreaInsets.bottom, right: 0)
        }
        
        secret.scrollIndicatorInsets = secret.contentInset
        
        let selectedRange = secret.selectedRange
        secret.scrollRangeToVisible(selectedRange)
    }
    
    func unlockSecretMessage(){
        secret.isHidden = false
        navigationItem.rightBarButtonItem?.isEnabled = true
        title = "Secret Stuff"
        
        secret.text = KeychainWrapper.standard.string(forKey: "SecretMessage") ?? ""
    }
    
    @objc func saveSecretMessage(){
        guard secret.isHidden == false else { return }
        
        navigationItem.rightBarButtonItem?.isEnabled = false
        
        KeychainWrapper.standard.set(secret.text, forKey: "SecretMessage")
        secret.resignFirstResponder()
        secret.isHidden = true
        title = "Nothing To See Here"
    }
    
    func promptForPassword(){
        let ac = UIAlertController(title: "Authentication Failed", message: "You could not be verified; please try with the app password.", preferredStyle: .alert)
        ac.addTextField()
        
        let submitAction = UIAlertAction(title: "Submit", style: .default) { [weak self, weak ac](_) in
            if ac?.textFields?[0].text != KeychainWrapper.standard.string(forKey: "Password") ?? "danni" {
                let ad = UIAlertController(title: "Unable to authenticate", message: "Bad Credentials", preferredStyle: .alert)
                ad.addAction(UIAlertAction(title: "OK", style: .default))
                self?.present(ad, animated: true)
            } else {
                self?.unlockSecretMessage()
            }
        }
        ac.addAction(submitAction)
        present(ac, animated: true)
    }
    
}

